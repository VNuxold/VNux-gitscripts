#!/bin/bash

~/git/VNux/VNux-gitscripts/pullall.sh
echo "-> Commiting all repos at ~/git/VNux..."
cd ~/git/VNux
cd VNux-Artwork
git add -A
git commit
git push
cd ..
cd VNux-Assistant
git add -A
git commit
git push
cd ..
cd VNux-gitscripts
git add -A
git commit
git push
cd ..
cd VNux-iso
git add -A
git commit
git push
cd ..
cd VNux-keyring
git add -A
git commit
git push
cd ..
cd VNux-PKGBUILD
git add -A
git commit
git push
cd ..
cd VNux-repo
git add -A
git commit
git push
cd ..
cd VNux-updatescripts
git add -A
git commit
git push
cd ..
cd VNux-website
git add -A
git commit
git push
cd ..
cd VNux-wiki
git add -A
git commit
git push
cd ..
cd VNux-dev-wiki
git add -A
git commit
git push
cd ..
cd VNux-Applications/calamares
git add -A
git commit
git push
cd ..
cd lxde-common
git add -A
git commit
git push
cd ..
cd lxpanel
git add -A
git commit
git push
cd ../../