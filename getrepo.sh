#!/bin/bash

echo "-> Cloning all repos at ~/git/VNux..."
mkdir -p ~/git
mkdir -p ~/git/VNux
cd ~/git/VNux
git clone https://gitlab.com/VNux/VNux-Artwork.git
git clone https://gitlab.com/VNux/VNux-Assistant.git
git clone https://gitlab.com/VNux/VNux-gitscripts.git
git clone https://gitlab.com/VNux/VNux-iso.git
git clone https://gitlab.com/VNux/VNux-keyring.git
git clone https://gitlab.com/VNux/VNux-PKGBUILD.git
git clone https://gitlab.com/VNux/VNux-repo.git
git clone https://gitlab.com/VNux/VNux-updatescripts.git
git clone https://gitlab.com/VNux/VNux-website.git
git clone https://gitlab.com/VNux/VNux-wiki.git
git clone https://gitlab.com/VNux/VNux-dev-wiki.git
mkdir -p VNux-Applications
cd VNux-Applications
git clone -b VNux https://github.com/th1nhhdk/calamares.git
git clone https://github.com/th1nhhdk/lxde-common.git
git clone https://github.com/th1nhhdk/lxpanel.git
cd ..
echo "-> Done!"
