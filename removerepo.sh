#!/bin/bash

function yes_or_no {
    while true; do
        read -p "$* [y/n]: " yn
        case $yn in
            [Yy]*) exit 0  ;;
            [Nn]*) echo "-> Đã hủy" ; exit 1 ;;
        esac
    done
}
if [[ -d ~/git/VNux ]]
then
    yes_or_no "-> Bạn có chắc không? (sẽ xóa ~/git/VNux)" && rm -rf ~/git/VNux
    exit 0
else
    echo '-> Không tìm thấy thư mục ~/git/VNux !'
    exit 1
fi

